package ru.tsc.avramenko.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.tsc.avramenko.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@RequestMapping("/api/tasks")
public interface ITaskEndpoint {

    @WebMethod
    @GetMapping("/find/{id}")
    Task find(
            @WebParam(name = "id")
            @PathVariable("id") final String id
    );

    @WebMethod
    @GetMapping("/findAll")
    Collection<Task> findAll();

    @WebMethod
    @PostMapping("/create")
    Task create(
            @WebParam(name = "task")
            @RequestBody final Task task
    );

    @WebMethod
    @PostMapping("/createAll")
    List<Task> createAll(
            @WebParam(name = "tasks")
            @RequestBody final List<Task> tasks
    );

    @WebMethod
    @PostMapping("/save")
    Task save(
            @WebParam(name = "task")
            @RequestBody final Task task
    );

    @WebMethod
    @PostMapping("/saveAll")
    List<Task> saveAll(
            @WebParam(name = "tasks")
            @RequestBody final List<Task> tasks
    );

    @WebMethod
    @PostMapping("/delete/{id}")
    void delete(
            @WebParam(name = "id")
            @PathVariable("id") final String id
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll();

}