package ru.tsc.avramenko.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.CustomerUser;

import java.util.Optional;

public class UserUtil {

    public static String getUserId() {
        @NotNull final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        @Nullable Object principal = auth.getPrincipal();
        Optional.ofNullable(principal).orElseThrow(ProcessException::new);
        if (!(principal instanceof CustomerUser)) {
            throw new ProcessException();
        }
        @NotNull CustomerUser customerUser = (CustomerUser) principal;
        return customerUser.getUserId();
    }

}