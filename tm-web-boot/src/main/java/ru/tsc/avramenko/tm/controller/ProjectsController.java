package ru.tsc.avramenko.tm.controller;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.avramenko.tm.service.ProjectService;
import ru.tsc.avramenko.tm.util.UserUtil;

@Controller
public class ProjectsController {

    @Autowired
    private ProjectService projectService;

    @GetMapping("/projects")
    public ModelAndView index() {
        @Nullable final String userId = UserUtil.getUserId();
        return new ModelAndView("project-list", "projects", projectService.findAllByCurrentUserId(userId));
    }

}