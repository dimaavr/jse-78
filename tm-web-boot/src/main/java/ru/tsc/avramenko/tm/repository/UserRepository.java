package ru.tsc.avramenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.tsc.avramenko.tm.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    @Query("SELECT e FROM User e WHERE e.id = :id")
    @Nullable User findUserById(
            @Param("id") @NotNull String id
    );

    @Modifying
    @Query("DELETE FROM User e")
    void clear();

    @Query("SELECT e FROM User e WHERE e.login = :login")
    @Nullable User findByLogin(
            @Param("login") @NotNull String login
    );

    @Query("SELECT e FROM User e WHERE e.email = :email")
    @Nullable User findByEmail(
            @Param("email") @NotNull String email
    );

    @Query("SELECT e FROM User e WHERE e.email = :email and e.id != :userId")
    @Nullable User findByEmailCurrentUser(
            @Param("userId") @NotNull String userId,
            @Param("email") @NotNull String email
    );

    @Modifying
    @Query("DELETE FROM User e WHERE e.login = :login")
    void removeUserByLogin(
            @Param("login") @NotNull String login
    );

}