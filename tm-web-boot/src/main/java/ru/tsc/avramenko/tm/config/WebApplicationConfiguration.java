package ru.tsc.avramenko.tm.config;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.tsc.avramenko.tm.endpoint.AuthEndpoint;
import ru.tsc.avramenko.tm.endpoint.ProjectEndpoint;
import ru.tsc.avramenko.tm.endpoint.TaskEndpoint;
import ru.tsc.avramenko.tm.endpoint.UserEndpoint;

import javax.xml.ws.Endpoint;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebApplicationConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private Bus bus;

    @Bean
    @NotNull
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    public AuthenticationManager authenticationManagerBean()
            throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/services/*").permitAll()
                .and()
                .authorizeRequests()

                .anyRequest().authenticated()
                .and()
                .formLogin()
                .and()
                .logout().permitAll()
                .logoutSuccessUrl("/")
                .and()
                .csrf().disable();
    }

    @Bean
    public Endpoint projectEndpointRegistry(final ProjectEndpoint projectEndpoint, final Bus bus) {
        final Endpoint endpoint = new EndpointImpl(bus, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(final TaskEndpoint taskEndpoint, final Bus bus) {
        final Endpoint endpoint = new EndpointImpl(bus, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint userEndpointRegistry(final UserEndpoint userEndpoint, final Bus bus) {
        final Endpoint endpoint = new EndpointImpl(bus, userEndpoint);
        endpoint.publish("/UserEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint authEndpointRegistry(final AuthEndpoint authEndpoint, final Bus bus) {
        final Endpoint endpoint = new EndpointImpl(bus, authEndpoint);
        endpoint.publish("/AuthEndpoint");
        return endpoint;
    }

}